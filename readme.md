# Google no doodle

Userscript for [Greasemonkey](https://addons.mozilla.org/ru/firefox/addon/greasemonkey/) / [Tampermonkey](https://www.tampermonkey.net/) that disable doodles on google website. A browser addon that disable doodles on google website.

[Go to website](https://winns.gitlab.io/userscript-google-no-doodle/).