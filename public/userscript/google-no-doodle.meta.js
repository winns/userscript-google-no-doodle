// ==UserScript==
// @name        Google no doodle
// @description Disable doodles on google website
// @copyright   2019, Pavel Sokolov
// @license   	MPL-2.0
// @author      Pavel Sokolov
// @version     1.0.1
// @homepageURL https://winns.gitlab.io/userscript-google-no-doodle
// @downloadURL https://winns.gitlab.io/userscript-google-no-doodle/userscript/google-no-doodle.meta.js
// @updateURL   https://winns.gitlab.io/userscript-google-no-doodle/userscript/google-no-doodle.user.js
// @supportURL  https://gitlab.com/winns/userscript-google-no-doodle
// @grant       none
// @include     *//www.google.*
// @include     *//google.*
// @run-at      document-start
// @namespace   winns
// ==/UserScript==