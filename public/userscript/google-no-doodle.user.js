// ==UserScript==
// @name        Google no doodle
// @description Disable doodles on google website
// @copyright   2019, Pavel Sokolov
// @license   	MPL-2.0
// @author      Pavel Sokolov
// @version     1.0.1
// @homepageURL https://winns.gitlab.io/userscript-google-no-doodle
// @downloadURL https://winns.gitlab.io/userscript-google-no-doodle/userscript/google-no-doodle.meta.js
// @updateURL   https://winns.gitlab.io/userscript-google-no-doodle/userscript/google-no-doodle.user.js
// @supportURL  https://gitlab.com/winns/userscript-google-no-doodle
// @grant       none
// @include     *//www.google.*
// @include     *//google.*
// @run-at      document-start
// @namespace   winns
// ==/UserScript==

(function() {
	var arScripts = [];

   	arScripts.push({
		isEnable: function() {
			return document.querySelector( '#body' ) !== null;
		},
		script: function() {
			var el = {
				wrapper: document.querySelector( '#body' )
			};

			this.run = function() {
				var html = '';

				html += '<div style="margin: 150px auto 0 auto; max-width: 320px; text-align: center;">';
				html += 	'<img src="https://upload.wikimedia.org/wikipedia/commons/2/2f/Google_2015_logo.svg" style="max-width: 100%;">';
				html += '</div>';

				el.wrapper.innerHTML = html;
			};
		}
	});

   	arScripts.push({
		isEnable: function() {
			return location.pathname.includes('/search');
		},
		script: function() {
			var el = {
				wrapper: document.querySelector( '.doodle' )
			};

			this.run = function() {
				var html = '';

				html += '<a href="//'+ location.host +'">';
				html += 	'<img src="https://upload.wikimedia.org/wikipedia/commons/2/2f/Google_2015_logo.svg" style="width: 92px; max-width: 100%;">';
				html += '</div>';

				el.wrapper.innerHTML = html;
			};
		}
	});

	// Run the scripts
	for (var i=0, s; i < arScripts.length; i++) {
		s = arScripts[i];

		if (s.isEnable()) (new s.script()).run();
	}
})();